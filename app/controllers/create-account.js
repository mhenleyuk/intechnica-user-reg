import Ember from 'ember';

export default Ember.Controller.extend({
  username: "",
  password: "",
  errors: Ember.A(),

  actions: {
      clearErrors() {
          this.set('errors', Ember.A());
      }
  }
});
