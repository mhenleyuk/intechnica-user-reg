import Ember from 'ember';

export default Ember.Route.extend({

    actions: {
        reset() {

            this.transitionTo('index');
        }
    }
});
