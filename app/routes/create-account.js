/* global zxcvbn */

import Ember from 'ember';

export default Ember.Route.extend({

  resetController(controller, isExiting, transition) {
    if (isExiting) {
      controller.set('username', "");
      controller.set('password', "");
      controller.set('errors', Ember.A());
    }
  },

  buildError(errors, target, label) {
    let error = {
      target,
      label
    };
    errors.addObject(error);
  },

  actions: {

    validateAccount() {

      let username = this.get('controller.username');
      let password = this.get('controller.password');
      let errors = [];

      if (username.length === 0) {
        this.buildError(errors, "username", "A username is required");
      }
      if (password.length === 0) {
        this.buildError(errors, "password", "A password is required");
      }
      if (zxcvbn(password).score <= 2) {
        this.buildError(errors, "password", "The password requirements haven't been met");
      }

      if (errors.length === 0) {
        return this.transitionTo('welcome');
      }
      else {
          console.log(errors);
          this.set('controller.errors', errors);
      }

    }

  }
});
