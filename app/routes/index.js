import Ember from 'ember';

export default Ember.Route.extend({

    beforeModel(transition) {
        return this.replaceWith('create-account');
    }
});
