import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('misc/password-strength-bar', 'Integration | Component | misc/password strength bar', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{misc/password-strength-bar}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#misc/password-strength-bar}}
      template block text
    {{/misc/password-strength-bar}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
