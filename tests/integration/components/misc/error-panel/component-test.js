import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('misc/error-panel', 'Integration | Component | misc/error panel', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{misc/error-panel}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#misc/error-panel}}
      template block text
    {{/misc/error-panel}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
