/* global zxcvbn */

import Ember from 'ember';
import layout from './template';

const VERY_STRONG = 4;
const STRONG = 3;
const MEDIUM = 2;
const LOW = 1;
const VERY_LOW = 0;

export default Ember.Component.extend({
  layout,
  classNames: ['strength-bar'],
  classNameBindings: ['status'],
  attributeBindings: ['dataSource'],
  data: "",
  dataSource: null,
  minValue: 0,
  maxValue: 4,
  value: 0,
  status: Ember.computed.alias('strength.type'),

  dataSourceDidChange: Ember.observer('dataSource', function () {
    this.set('data', zxcvbn(this.get('dataSource')));
  }),

  strength: Ember.computed('data.score', function () {

    /*
  0 # too guessable: risky password. (guesses < 10^3)
  1 # very guessable: protection from throttled online attacks. (guesses < 10^6)
  2 # somewhat guessable: protection from unthrottled online attacks. (guesses < 10^8)
  3 # safely unguessable: moderate protection from offline slow-hash scenario. (guesses < 10^10)
  4 # very unguessable: strong protection from offline slow-hash scenario. (guesses >= 10^10)
    */

    let value = this.get('data.score');
    this.set('value', value);
    let strength = value,
      label = "Too weak",
      type;

    switch (value) {
      default:
        case 1:
        type = "weak";
      label = 'Too weak';
      break;
      case MEDIUM:
          type = "medium";
        label = 'Could be stronger';
        break;
      case STRONG:
          case VERY_STRONG:
          type = "strong";
        label = 'Strong password';
        break;
    }

    let obj = {
      strength,
      type,
      label
    };

    return obj;
  }),


  /**
   * @property style
   */
  width: Ember.computed('strength', function () {

    let value = this.get('value');

    /*
    If the user hasn't entered a value, don't show it as a weak password.
    */
    if (this.get('dataSource').length === 0) {
      value = 0
    } else {
      /*
          If the value is either very low or low, then at least show the indicator.
          */
      switch (value) {
        case 0:
        case 1:
          value = 1
          break;
        case 3:
        case 4:
          value = 4
          break;
      }
    }

    console.log(value);

    let minValue = parseFloat(this.get('minValue'));
    let maxValue = parseFloat(this.get('maxValue'));
    let percent = Math.min(Math.max((value - minValue) / (maxValue - minValue), 0), 1) * 100;
    return new Ember.String.htmlSafe(`width: ${percent}%`);
  })

});
