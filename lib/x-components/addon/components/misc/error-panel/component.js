import Ember from 'ember';
import layout from './template';

export default Ember.Component.extend({
  layout,
  classNames: ['errors','errors-panel'],
  attributeBindings: ['dataSource'],
  dataSource: null,

  actions: {
    destroy() {
      this.destroy();
      this.sendAction('clearErrors');
    }
  }
});
