import Ember from 'ember';
import layout from './template';

export default Ember.Component.extend({
  layout,
  classNames: ['x-input'],
  classNameBindings:['showRequired:has-error'],
  attributeBindings: ['label','type','value','placeholder'],
  value: null,
  inputLength: Ember.computed.alias('value.length'),
  label: '',
  type: "text",
  placeholder: "",
  showRequired: Ember.computed.lt('inputLength', 1)

  

});
