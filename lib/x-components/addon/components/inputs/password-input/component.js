import Ember from 'ember';
import layout from './template';
import XInput from '../x-input/component';

export default XInput.extend({
  layout,
  classNames: ['password-input'],
  classNameBindings: ['hasFocusedIn:focused'],
  validatePassword: Ember.computed.gt('inputLength', 0),
  showHide: {
    type: "password",
    label: "show"
  },
  showHideType: Ember.computed.alias('showHide.type'),
  showHideLabel: Ember.computed.alias('showHide.label'),

  // Built in event
  focusIn() {
    this.set('hasFocusedIn', true);
  },

  focusOut() {
    this.set('hasFocusedIn', false);
  },

  actions: {
    toggleType() {

      this.set('showHide', {
        type: (this.get('showHideType') == 'text' ? 'password' : 'text'),
        label: (this.get('showHideLabel') == 'show' ? 'hide' : 'show'),
      });
    }
  }
});
